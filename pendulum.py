#!/usr/bin/env python3

from support import *

m_A = 1
m_B = 2
l_AB = 1
l_OA = 2
G = (0, -10)
t_FRIC = 0.4
t_SLOW = 0.01
MAX_OUT = 15

# pendulum attached to a circle
def system(state, output, dt):
    alpha = state[0]
    theta = state[1]
    d_alpha = state[2]
    d_theta = state[3]
    t_ext = output[0]

    # calc
    loc_a = rtheta(l_OA, alpha)
    f_a = mul(G, m_A)
    t_a = deadzone(dot(f_a, rtheta(1, alpha + pi/2)) + t_ext, t_FRIC)

    # modification
    d2_alpha = (t_a / (m_A * l_OA * l_OA))
    d_alpha = deadzone(d_alpha + dt * d2_alpha, dt * t_SLOW)
    alpha += dt * d_alpha

    alpha = circbound(alpha)
    theta = circbound(theta)

    return [alpha,theta,d_alpha,d_theta]

integral = 0
def findcontrol(state, target, dt):
    global integral
    t_alpha = target[0]
    alpha = state[0]
    err = t_alpha - alpha
    integral += err * dt

    return [sbound(0, MAX_OUT)]

def run():
    leg_out = ["Output"]
    output = [0]
    # Alpha, Theta, dAlpha, dTheta
    leg_state = ["Alpha", "Theta", "dAlpha", "dTheta"]
    state = [pi / 3*0.99,pi / 2,0,0]
    target = [pi/3,pi/3,0,0]
    leg_target = ["Target Alpha","Target Theta","Target dAlpha","Target dTheta"]
    ts = 0.05

    history = []
    for i in range(1000):
        state = system(state, output, ts)
        output = findcontrol(state, target,ts)
        history.append([i * ts] + state + output + target)

    tg = swap_indices(history)

    legends = ["Time"] + leg_state + leg_out + leg_target
    for i in range(1, len(tg)):
        plt.plot(tg[0], tg[i], label=legends[i])
    plt.legend()
    try:
        plt.show()
    except KeyboardInterrupt:
        pass

if __name__ == "__main__":
    run()



