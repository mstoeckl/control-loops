#!/usr/bin/env python3

from support import *

armA_l = 0.40 # m
armA_th_off = 5 * degToRad # deg ; base + th_off = link
armA_m = 5.00 # kg

link_armA = 0.15 #m
link_armB = 0.20 #m
link_ground = 0.40 #m
link_ground_angle = 30 * degToRad # deg
link_top = 0.30 #m

armB_l = 0.20 # m
armB_th_off = -20 * degToRad # deg; base + th_off = link
armB_m = 2.30 # kg

gravity = (0, -10)

fric_first = 0.0002
fric_second = 0.0002

LOWER = 40*degToRad
UPPER = 145*degToRad

def calcArmTorque(length, mass, angle):
    force = mul(gravity, mass)
    # eqv. to cross product, a rotate by 90 (sin <-> cos)
    torque = dot(force, rtheta(length, angle + pi/2))
    return torque

def solveQuadratic(A,B,C):
    disc = B*B-4*A*C
    if disc < 0:
        return []
    return [(-B + sqrt(disc)) / (2 * A), (-B - sqrt(disc)) / (2 * A)]


# returns list of solutions
def solve2Bar(p_a, p_b, d_a, d_b):
    K = (-2 + p_a[0] + p_b[0])*(p_a[0] - p_b[0])
    J = (-2 + p_a[1] + p_b[1])*(p_a[1] - p_b[1])
    L = d_a * d_a - d_b * d_b

    A = (J/K)**2 - 1
    B = -2 * J/K*p_b[0] - 2*p_b[1]
    C = (L/K) ** 2 + p_b[0] ** 2 + p_b[1] ** 2 - d_b ** 2 - 2*L/K*p_b[0]

    ys = solveQuadratic(A,B,C)
    return [ ((L - y*J) / K,y) for y in ys ]

def alphaAtoB(angle):
    loc_a = rtheta(link_armA, angle + armA_th_off)
    loc_g = rtheta(link_ground, link_ground_angle)

    poss_loc_f = solve2Bar(loc_a, loc_g, link_top, link_armB)
    assert len(poss_loc_f) > 0
    loc_f = max(poss_loc_f, key=lambda x: x[1])

    link_b = atan2(loc_f[1] - loc_g[1], loc_f[0] - loc_g[0])
    return link_b - armB_th_off

# use with anything proportional to
def dAlphaAtoB(angle, dalpha):
    e = 0.001
    return (alphaAtoB(angle+e) - alphaAtoB(angle-e)) / 2 / e * dalpha

def dAlphaBtoA(angle, dalpha):
    return 1 / dAlphaAtoB(angle, dalpha)

def d2AlphaAtoB(angle, dalpha, d2alpha):
    e = 0.001
    return (dAlphaAtoB(angle+e,dalpha) - dAlphaAtoB(angle-e, dalpha)) / 2 / e * d2alpha

def getRodMOI(leng, mass):
    return leng * leng * mass / 3

def calcBandTorqueB(angle):
    # TODO: rubber band torque on B
    return 0

def getAbsFrictionTorqueB(angle, d_angle, d2_angle):
    return abs(fric_first * d_angle) + abs(fric_second * d2_angle)

def system(state, output, dt):
    angle = state[0]
    d_angle = state[1]
    d2_angle = state[2]

    torqueA = calcArmTorque(armA_l, armA_m, angle)

    angleB = alphaAtoB(angle)

    torqueB = calcArmTorque(armB_l, armB_m, angleB)

    torqueRubberBand = calcBandTorqueB(angle)

    torqueBNet = torqueB + dAlphaAtoB(angle, torqueA) + torqueRubberBand

    torqueAnti = getAbsFrictionTorqueB(angleB, dAlphaAtoB(angle, d_angle), d2AlphaAtoB(angle, d_angle, d2_angle))
    torqueBNet = deadzone(torqueBNet, abs(torqueAnti))


    # torque resisting motion (ax' + bx'')

    # then accelerate it all

    moiA_B = dAlphaAtoB(angle, getRodMOI(armA_l, armA_m))
    moiB_B = getRodMOI(armB_l, armB_m)

    moiNetB = moiA_B + moiB_B

    accB = torqueBNet / moiNetB
    accA = dAlphaBtoA(angle, accB)

    # update
    d2_angle = state[2]
    d_angle += accA * dt
    angle += d_angle * dt

    if (angle <= LOWER):
        angle = LOWER
        d2_angle = (d_angle - (accA * dt)) / dt
        d_angle = 0.0
    if angle >= UPPER:
        angle = UPPER
        d2_angle = (d_angle - (accA * dt)) / dt
        d_angle = 0.0

    return [angle,d_angle, d2_angle]

def findcontrol(state, target, dt):
    x = state[0]
    v = state[1]
    a = state[2]
    tx = target[0]
    tv = target[1]
    ta = target[2]

    out = 0

    return [sbound(out, 1)]

def printState(state):
    print("{} {} {}".format(state[0] / degToRad, state[1] / degToRad, state[2] / degToRad))

def run():
    leg_out = ["Output"]
    output = [0]
    # Alpha, Theta, dAlpha, dTheta
    leg_state = ["Alpha", "dAlpha/dt", "d2Alpha/dt2"]
    state = [100*degToRad, 0, 0]
    target = [100*degToRad,0, 0]
    leg_target = ["Target Alpha","Target dAlpha/dt","Target d2Alpha/dt2"]
    ts = 0.05

    history = []
    for i in range(10):
        state = system(state, output, ts)
        printState(state)
        output = findcontrol(state, target,ts)
        history.append([i * ts] + state + output + target)

    tg = swap_indices(history)

    legends = ["Time"] + leg_state + leg_out + leg_target
    for i in range(1, len(tg)):
        plt.plot(tg[0], tg[i], label=legends[i])
    plt.legend()
    try:
        plt.show()
    except KeyboardInterrupt:
        pass

if __name__ == "__main__":
    run()
