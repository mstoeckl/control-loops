from math import *
from random import *
from matplotlib import pyplot as plt

degToRad = pi / 180

def add(v_1, v_2):
    return (v_1[0]+v_2[0],v_1[1]+v_2[1])
def rtheta(r, theta):
    return (cos(theta) * r, sin(theta) * r)
def wavg(a, w_a, b, w_b):
    return ((a[0] * w_a + b[0] * w_b) / (w_a + w_b), (a[1] * w_a + b[1] * w_b) / (w_a + w_b))
def getr(p):
    return sqrt(p[0]**2 + p[1]**2)
def mul(v, s):
    return (v[0] * s, v[1] * s)
def dot(a, b):
    return a[0] * b[0] + a[1] * b[1]
def proj(a, b):
    return mul(b, dot(a, b))

def circbound(x):
    r = x / pi
    if r > 1:
        return x - (int(r+1) * (pi))
    if r < -1:
        return x - (int(r-1) * (pi))
    return x
def sbound(x, r):
    if x > r:
        return r
    if x < -r:
        return -r
    return x
def deadzone(x, e):
    if x > e:
        return x - e
    if x < -e:
        return x + e
    return 0

def swap_indices(arr):
    l = [[] for x in range(len(arr[0]))]
    for a in range(len(arr[0])):
        for b in range(len(arr)):
            l[a].append(arr[b][a])
    return l




