#!/usr/bin/env python3

import graph
import time
from math import pi
import scipy.signal as sig
import numpy as np

data = graph.data



def diff(seqy, seqx):
    """
        Backward differentiation seqy w.r.t seqx.
        The first value is 0.
    """
    o = [0]
    y = iter(seqy)
    x = iter(seqx)
    last_x = next(x)
    last_y = next(y)
    while True:
        try:
            v_x = next(x)
            v_y = next(y)
        except StopIteration:
            break

        dx = v_x - last_x
        if dx == 0:
            o.append(0)
        else:
            o.append((v_y - last_y) / dx)
        last_y = v_y
        last_x = v_x

    return o

def shift(seq, offset):
    if offset >= 0:
        return [0] * offset + seq[:len(seq)-offset]
    else:
        return seq[-offset:] + [0] * (-offset)

def lowpass(seq, count=30):
    """
        Lowpass filter the sequence.
    """
    back = [0] * count
    o = []
    l = 0
    for v in seq:
        back[l] = v
        o.append(sum(back) / count)
        l = (l + 1) % count

    return o

def scale(seq, factor):
    return list(map(lambda x: x * factor, seq))

def motor(pseq, lseq, tseq):
    #
    # V = L*I' + RI + ka'
    # Ma'' = jI - va' - T
    #
    # V - voltage
    # L - inductance
    # I - current
    # R - resistance
    # k - back emf constant
    # a'- ang vel
    # a'' - ang acc
    # M - moi of rotor
    # j - torque constant
    # T - external load torque
    # v - viscous friction

    V = 12.00

    # free vel =from stall cur 75 rpm
    # free cur = 0.6 A
    # stall cur = 22 A
    # stall torq = 22.51 N-m

    cur_stall = 22
    torq_stall = 22.51

    cur_free = 0.6
    vel_free = 75 * 60 * (2 * pi)

    # stall
    R = V / cur_stall
    k_t = torq_stall * R / V
    k_b = (V - R * cur_free) / (vel_free)
    v = torq_stall / vel_free - k_t*k_b/R

    # inductance (L)
    # rotor mass (M)

    L = 0.011 # Henry
    M = 0.011 # kg / m^2

    #T =  # calc

    # the actual loop

    I = 0

    # intake scale aseq

    # 0-1=100 deg
    lseq = scale(lseq, (100 / 360) * 2 * pi)
    vseq = diff(lseq,tseq)
    aseq = diff(vseq,tseq)
    last_time = 0
    b = 17/100000000 # damping
    out = []
    for p,l,v,a,t in zip(pseq,lseq,vseq,aseq,tseq):
        V_i = p * V
        dt = t - last_time
        last_time = t

        dI = (V_i - R * I - k_b * v) * dt / L
        #print(V_i, R, I, k_b, v, dt, L, dI)
        tau = k_t * I - b*v
        I += dI
        #print(tau, I, b, v)
        out.append(tau)







    return out

def smooth(seq, cutoff=0.02):
    f1,f2 = sig.butter(4, cutoff, 'low')
    o = sig.filtfilt(f1,f2,seq)
    return o

def runavg(seq, length=5):
    data = np.arange(1,11)
    w = [1.0/length]*length
    return np.correlate(seq,w,'same')

def normalize(name):
    """ 0 preserving linear normalization """
    d = data[name]
    m = max(abs(max(d)),abs(min(d)))
    u = [ x / m for x in d  ]
    data[name] = u

def main():

    graph.load_data("tmp.txt")
    graph.load_data("react.txt")
    t = 0
    for _ in range(len(data.loc)):
        data.time << t
        t += 0.050
    normalize("vel")


    graph.show("loc","out","vel","F")

    #graph.show("F", base="loc")

    #graph.surf3d("sloc", "npow", "sacc")

    graph.end()


if __name__ == "__main__":
    print("Loading")
    if True:
        main()
    else:
        import statprof
        statprof.start()
        main()
        statprof.stop()
        statprof.display()






