#!/usr/bin/env python3

import graph
from enum import Enum
from math import sqrt
from random import gauss
from time import time,sleep
from re import split as rsplit

data = graph.data

def is_stalled(v):
    return abs(v) < 0.01

def arange(min,max,step):
    while min < max:
        yield min
        min += step

def sbound(x,r):
    return sign(x) * min(abs(x),r)

def wmerge(a, b, w=1):
    assert w != -1
    return (a + b * w) / (1.0 + w)

def sign(x):
    if x > 0:
        return 1
    if x < 0:
        return -1
    return 0

def pprint(names,*args):
    n = names.split(" ")
    assert len(n) == len(args)
    def get_code(u):
        if isinstance(u, float):
            return ": 2.4f"
        if isinstance(u, ABS):
            return ".name"
        return ""
    st = " | ".join([n[i]+": {" + get_code(args[i]) + "}" for i in range(len(n))])
    print(st.format(*args))

class glob():
    t = 0
    @staticmethod
    def time():
        return glob.t

# CONTROLLERS

def seek_convex(inst, min, max, thunk):
    score_max = thunk(inst, max)
    score_min = thunk(inst, min)
    for i in range(18):
        low = (min * 2 + max) * 0.333333333333
        high = (max * 2 + min) * 0.333333333333
        score_high = thunk(inst, high)
        score_low = thunk(inst, low)
        if score_high >= score_low:
            max = high
            score_max = score_high
        if score_low >= score_high:
            min = low
            score_min = score_low
    return (max + min) / 2.0

class RingBuffer():
    def __init__(self, size):
        self.length = size + 1
        self.buf = [0]*self.length
        self.tip = self.tail = 0
    def clear(self):
        self.tip = self.tail
    def __getitem__(self, idx):
        addr = idx + self.tail
        if addr >= self.length:
            return self.buf[addr - self.length]
        return self.buf[addr]
    def next(self, v):
        self.buf[self.tip] = v
        self.tip += 1
        if self.tip >= self.length:
            self.tip = 0
        if self.tip == self.tail:
            self.tail += 1
            if self.tail >= self.length:
                self.tail = 0

    def size(self):
        if self.tip >= self.tail:
            return self.tip - self.tail
        return self.tip - self.tail + self.length

class MovingAverageFilter():
    def __init__(self, size):
        self.buf = RingBuffer(size)
    def recalc(self):
        s = 0
        for i in range(self.buf.size()):
            s += self.buf[i]
        return s / self.buf.size()
    def calc(self, next):
        self.buf.next(next)
        return self.recalc()
    def reset(self, v):
        self.buf.clear()
        self.buf.next(v)

class State():
    def __init__(self, x, v):
        self.x = x
        self.v = v
    x = 0
    v = 0

OUT_SCALE = 15.0
K_PROP = 2.5
STALL_SPEED = 0.01
FRIC_STATIC = 0.2
FRIC_DYNAMIC = 0.1
PREDICTION_STEP = 0.05

def within(x,r):
    return x < r and x > -r

def sub_predict(state, output, F, time):
    force = (output + F) * OUT_SCALE
    if within(state.v, STALL_SPEED) and within(force, FRIC_STATIC):
        return State(state.x, 0)
    fdyn = FRIC_DYNAMIC * sign(state.v)
    acc = force - fdyn
    v = state.v + acc * time
    x = state.x + v * time
    if x > 1.0:
        return State(1.0,0.0)
    if x < 0.0:
        return State(0.0,0.0)
    return State(x,v)

def predict(state, output, F, time):
    B = 0.0125
    ts = time / round(time / B)
    for i in range(round(time / B)):
        state = sub_predict(state, output, F, ts)
    return state

class ModelController():
    def __init__(self):
        self.filtF = MovingAverageFilter(1)
        self.output_smooth = MovingAverageFilter(1)
        self.hist_v = RingBuffer(5)
        self.hist_x = RingBuffer(5)
        self.hist_out = RingBuffer(5)
        self.hist_ts = RingBuffer(5)
    def target_speed(self, loc):
        return (self.target_x - loc) * K_PROP
    def reset(self, loc):
        self.estF = 0
        self.filtF.reset(0)
        self.output_smooth.reset(0)
        self.hist_v.clear()
        self.hist_x.clear()
        self.hist_out.clear()
        self.current = State(loc, 0)
        self.time = glob.time()
        self.ts = 0.050
    def __call__(self,loc,target,last_out):
        self.hist_x.next(self.current.x)
        self.hist_v.next(self.current.v)
        self.hist_out.next(last_out)
        self.hist_ts.next(self.ts)

        ntime = glob.time()
        self.ts = ntime - self.time
        self.time = ntime

        self.current = State(loc, (loc - self.current.x) / self.ts)
        self.target_x = target

        self.estF = self.filtF.calc(seek_convex(self, -1.0, 1.0, ModelController.eval_F))

        output = self.output_smooth.calc(seek_convex(self, -1.0,1.0,ModelController.eval_pow))

        data.estF << self.estF
        data.loc << self.current.x
        data.vel << self.current.v

        return output
    @staticmethod
    def eval_F(ctr, F):
        state = State(ctr.hist_x[0], ctr.hist_v[0])
        cost = 0.0
        len = ctr.hist_x.size()
        for i in range(len-1):
            out = ctr.hist_out[i]
            state = predict(state, out, F,ctr.hist_ts[i+1])
            err = state.x - ctr.hist_x[i+1]
            cost += (i+1) * err * err
        state = predict(state, ctr.hist_out[len - 1], F, ctr.hist_ts[len - 1])
        err = state.x - ctr.current.x
        cost += len * err * err
        return cost
    def eval_pow(ctr, pow):
        cost = 0.0
        state = ctr.current
        for i in range(3):
            state = predict(state, pow, ctr.estF, 0.050)
            err = ctr.target_speed(state.x) - state.v
            cost += (i+1) * err * err
        return cost

class Timer():
    def __init__(self, on=True):
        self.on = on
    def __enter__(self):
        self.t = time()
    def __exit__(self, _1, _2, _3):
        if self.on:
            pprint("t", time() - self.t)


class PIDController():
    # Ku = 2.0
    # Pu = 4.4
    def __init__(self, P=1.2, I=0.9, D=1.1):
        self.P = P
        self.I = I
        self.D = D
        self.integral = 0
        self.err = 0
    def reset(self, loc):
        self.err = 0
        self.integral = 0
    def __call__(self,loc,target,last_out):
        err = target - loc
        self.integral += err * 0.050
        diff = (err - self.err) / 0.050
        self.err = err

        data.estF << 1
        data.loc << loc
        data.vel << 1

        return self.P * err + self.I * self.integral + self.D * diff

class ABS(Enum):
    UP = 1
    DOWN = 2
    IDK = 3
    @property
    def name(self):
        return str(self) + " "*max(0,8-len(str(self)))

class AntiBacklash():
    def reset(self, loc):
        self.sensor = loc
        self.state = ABS.IDK
        self.last_state = ABS.IDK
        self.radius = 0.05
        self.last = 0
        self.locktime = 0
        self.pulse = False
        self.lastout = 0
    def __call__(self,loc,target,last_out):
        if self.state == ABS.IDK and self.last_state == ABS.IDK:
            assumed = loc
        elif self.state == ABS.UP or self.last_state == ABS.UP:
            assumed = loc + self.radius
        elif self.state == ABS.DOWN or self.last_state == ABS.DOWN:
            assumed = loc - self.radius
        else:
            raise Exception("BAD STATE")

        data.assumed << assumed

        if self.state == ABS.UP and assumed < self.last:
            self.locktime = glob.time()
            self.state = ABS.IDK
            self.last_state = ABS.UP
        elif self.state == ABS.DOWN and assumed > self.last:
            self.locktime = glob.time()
            self.state = ABS.IDK
            self.last_state = ABS.DOWN
        elif self.state == ABS.IDK and abs(assumed - self.last) > 0.01:
            self.last_state = ABS.IDK
            if assumed > self.last:
                self.state = ABS.UP
            else:
                self.state = ABS.DOWN
        vel = (assumed - self.last) / 0.050
        self.last = assumed

        pprint("S l a", self.state, loc, assumed)

        # phase lead
        future = predict(State(assumed, vel), self.lastout, 0, 0.05)
        err = (target - future.x) * 2.9
        if self.state == ABS.UP or self.state == ABS.DOWN:
            output = err
        else:
            boost = (glob.time() - self.locktime) * 0.1
            self.pulse = not self.pulse
            if self.pulse:
                output =  sign(err) * boost + err
            else:
                output =  0

        self.lastout = output
        return output

# SIMULATION

class Delayer():
    def __init__(self,length,seed):
        self.buf = RingBuffer(length+1)
        for k in range(length+1):
            self.buf.next(seed)
    def next(self, v):
        self.buf.next(v)
        return self.buf[0]

class Lagger():
    def __init__(self, radius):
        self.pos = 0
        self.R = radius
    def next(self, n):
        if n > self.pos + self.R:
            self.pos = n - self.R
        if n < self.pos - self.R:
            self.pos = n + self.R
        return self.pos

def run(controller):
    #
    # TODO: run simulation at speed beyond controller
    # so sim: 0.005
    #    cnt: 0.050
    #
    #
    STEP_BIG = 0.05
    STEP_SMALL = 0.005
    DELAY = 0.0125
    MAG_CONST = 10
    F_K = 0.05
    F_S = 0.10
    X_START = 0.2
    X_TARGET = 0.8
    BACKLASH = 0.05
    def model(state, output, ts):
        """
        Da real model. Definitely should not be like the actual model.
        """
        if state.x < 0.3:
            force = (0.3 - state.x) * -0.90 + 0.20
        elif state.x > 0.6:
            force = (state.x - 0.6) * 0.20 + 0.20
        else:
            force = 0.20
        if glob.t > 2.5 and glob.t < 2.8:
            force += -1.0

        o = (output + force) * MAG_CONST
        if is_stalled(state.v) and abs(o) < F_S:
            return State(state.x,0),force
        else:
            a = o - sign(state.v) * F_K

        v = state.v + a * ts
        x = state.x + v * ts
        if x > 1.0:
            return State(1.0,0.0),force
        if x < 0.0:
            return State(0.0,0.0),force
        return State(x, v),force

    output,realF = 0,0
    target = X_TARGET
    state = State(X_START,0)
    glob.t = -STEP_BIG
    controller.reset(state.x)
    sensor_x = state.x
    stepset = int(STEP_BIG / STEP_SMALL)
    delay = Delayer(int(DELAY * stepset / STEP_BIG),state.x)
    lag = Lagger(BACKLASH)
    for t in arange(0,5,STEP_BIG):
        glob.t = t
        output = sbound(controller(sensor_x,target,output),1)
        data.out << output
        data.realX << state.x
        data.sensorX << sensor_x
        data.realF << realF
        data.time << t
        data.target << target

        for i in range(stepset):
            state,realF = model(state, output, STEP_BIG / stepset)
            sensor_x = delay.next(lag.next(state.x)+gauss(0,0.002))

def rename(num):
    for field in data:
        o = rsplit("[^0-9]", field)
        if o and not o[-1]:
            data[field+str(num)] = data[field]
            del data[field]

def normalize(name):
    """ 0 preserving linear normalization """
    d = data[name]
    m = max(abs(max(d)),abs(min(d)))
    del data[name]
    for x in d:
        data[name] << x / m

def main():
    with Timer(False):
        run(PIDController())
    graph.show("realX","sensorX","target")

    graph.end()

if __name__ == "__main__":
    if True:
        main()
    else:
        import statprof
        statprof.start()
        main()
        statprof.stop()
        statprof.display()


